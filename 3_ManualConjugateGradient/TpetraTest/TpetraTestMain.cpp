#include <Tpetra_DefaultPlatform.hpp>
#include <Tpetra_Version.hpp>
#include <Teuchos_GlobalMPISession.hpp>

#include <Tpetra_Vector.hpp>
#include <Tpetra_CrsMatrix.hpp>

#include <boost/mpi.hpp>
#include <boost/timer.hpp>

int main()
{
    // MPI環境＆コミュニケーター
    boost::mpi::environment env(true);
    boost::mpi::communicator mpiCommunicator;
	auto comm = Teuchos::rcp(new Teuchos::MpiComm<int>(Teuchos::opaqueWrapper(static_cast<MPI_Comm>(mpiCommunicator))));
	
	// MPIプロセス番号
	const int rank = mpiCommunicator.rank();
	
	// ベクトル・行列の要素数
	const int n = 1000*90;
	const int maxNonZeroColumn = 10; // 行列の1行にある最大非ゼロ要素数（列数）

	if(rank == 0)
	{
		std::cout << "# of Process : " << mpiCommunicator.size() << std::endl;
		std::cout << "size of unknown variables : " << n << std::endl;
		std::cout << "Start!" << std::endl;
	}


	// データの分散方法を指定（連続：各プロセスに均等に連続に割り当てる方法）
	auto node = Kokkos::DefaultNode::getDefaultNode();
	const int indexBase = 0;
	auto map = Teuchos::rcp(new Tpetra::Map<>(n, indexBase, comm, Tpetra::GloballyDistributed, node));

	// 行列Aを作成
	auto A = Teuchos::rcp(new Tpetra::CrsMatrix<double>(map, maxNonZeroColumn));
	auto getGlobalIndex = map->getNodeElementList();

	// ベクトルxとbを作成
	auto x = Teuchos::rcp(new Tpetra::Vector<double>(map));
	auto b = Teuchos::rcp(new Tpetra::Vector<double>(map));

	// 行列作成開始
	A->resumeFill();

	// 自プロセスの担当する要素で
	const int nLocal = map->getNodeNumElements();
	for(int iLocal = 0; iLocal < nLocal; iLocal++)
	{
		// 行番号を取得
		const int i = getGlobalIndex[iLocal];

		// 2次中央差分の行列を作成する
		// （対角項が-2でその隣が1になる、↓こんな行列）
		// | -2  1  0  0  0  0  0  0 . . .  0  0  0|
		// |  1 -2  1  0  0  0  0  0 . . .  0  0  0|
		// |  0  1 -2  1  0  0  0  0 . . .  0  0  0|
		// |  0  0  1 -2  1  0  0  0 . . .  0  0  0|
		// |  0  0  0  1 -2  1  0  0 . . .  0  0  0|
		// |  0  0  0  0  1 -2  1  0 . . .  0  0  0|
		// |  0  0  0  0  0  1 -2  1 . . .  0  0  0|
		// |  0  0  0  0  0  0  1 -2 . . .  0  0  0|
		// |  .  .  .  .  .  .  .  . .      .  .  .|
		// |  .  .  .  .  .  .  .  .   .    .  .  .|
		// |  .  .  .  .  .  .  .  .     .  .  .  .|
		// |  0  0  0  0  0  0  0  0 . . . -2  1  0|
		// |  0  0  0  0  0  0  0  0 . . .  1 -2  1|
		// |  0  0  0  0  0  0  0  0 . . .  0  1 -2|

		const double a_ii = -2.0;
		const double a_ii1 = 1.0;

		// 0行目： a_00=-2, a_01=1
		if(i == 0)
		{
			A->insertGlobalValues(0, Teuchos::tuple(0, 1), Teuchos::tuple(a_ii, a_ii1));
		}
		// n行目： a_n(n-1)=1, a_nn=-2
		else if(i == n-1)
		{
			A->insertGlobalValues(n-1, Teuchos::tuple(n-2, n-1), Teuchos::tuple(a_ii1, a_ii));
		}
		// i行目： a_i(i-1)=1, a_ii=-2, a_i(i+1)=1
		else
		{
			A->insertGlobalValues(i, Teuchos::tuple(i-1, i, i+1), Teuchos::tuple(a_ii1, a_ii, a_ii1));
		}

		// b_i = 1.0
		b->replaceGlobalValue(i, 1.0);
	}

	// x_i = 0
	x->putScalar(0.0);
	

	// 行列作成完了
	A->fillComplete();

	const double allowableResitual = 1e-8;

	auto r = Teuchos::rcp(new Tpetra::Vector<double>(map));
	auto p = Teuchos::rcp(new Tpetra::Vector<double>(map));
	auto Ap = Teuchos::rcp(new Tpetra::Vector<double>(map));
	
	boost::timer timer;
	timer.restart();

	// 初期値を設定
	//  (Ap)_0 = A * x
	//  r_0 = b - Ap
	//  p_0 = r_0
	//  rr = r・r
	A->apply(*x, *Ap);
	r->update(1.0, *b, -1.0, *Ap, 0.0); // r = 1.0b + (-1.0)Ap + 0.0r
	p->assign(*r);
	double rr = r->dot(*r);
	const double residual0 = allowableResitual*allowableResitual;

	// 初期値で既に収束している場合は即時終了
	bool isConverged = (residual0 == 0);
	// 未知数分だけ繰り返す
	for(unsigned int i = 0; (i < n)&&(!isConverged); i++)
	{
		// 計算を実行
		//  Ap = A * p
		//  α = rr/(p・Ap)
		//  x' += αp
		//  r' -= αAp
		//  r'r' = r'・r'
		A->apply(*p, *Ap);
		const double alpha = rr / r->dot(*Ap);
		x->update(alpha, *p, 1.0);   // x = αp + 1.0x
		r->update(-alpha, *Ap, 1.0); // r = -αp + 1.0r
		const double rrNew = r->dot(*r);

		// 収束判定
		const double residual = rrNew;
		isConverged = (residual < residual0);

		// 収束していなければ、残りの計算を実行
		if(!isConverged)
		{
			// 残りの計算を実行
			//  β= r'r'/rr
			//  p = r' + βp
			//  rr = r'r'
			const double beta = rrNew / rr;
			p->update(1.0, *r, beta); // p = 1.0r + βp
			rr = rrNew;
		}
	}
	
	// 結果を通常のvectorに複製
	std::vector<double> result;
	result.resize(nLocal);
	auto xView = x->get1dView();
	std::copy_n(xView.begin(), nLocal, result.begin());
		
	// 最終結果をGather
	if(rank == 0)
	{
		std::vector<double> output;
		output.resize(n);

		boost::mpi::gather(mpiCommunicator, result.data(), nLocal, output.data(), 0);

		std::cout << "Time: " << timer.elapsed() << std::endl;

		for(int i = 0; i < n; i++)
		{
			// 期待値と違う場合にデータを表示
			const double expected = (i-n)*(i+1)/2.0;
			if(output[i] != expected)
			{
				std::cout << i << ":" << output[i] << ", expected=" << expected << std::endl;
			}
		}

		system("pause");
	}
	else
	{
		boost::mpi::gather(mpiCommunicator, result.data(), nLocal, 0);
	}

	return 0;
}
