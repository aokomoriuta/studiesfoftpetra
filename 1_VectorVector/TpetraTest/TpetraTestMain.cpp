#include <Tpetra_DefaultPlatform.hpp>
#include <Tpetra_Vector.hpp>
#include <Tpetra_Version.hpp>
#include <Teuchos_GlobalMPISession.hpp>
#include <Teuchos_oblackholestream.hpp>
#include <boost/mpi.hpp>

int main()
{
    // MPI環境＆コミュニケーター
    boost::mpi::environment env(true);
    boost::mpi::communicator mpiCommunicator;
	auto comm = Teuchos::rcp(new Teuchos::MpiComm<int>(Teuchos::opaqueWrapper(static_cast<MPI_Comm>(mpiCommunicator))));
	
	// MPIプロセスを表示
	const int size = mpiCommunicator.size();
	const int rank = mpiCommunicator.rank();
	std::cout << rank << "/" << size << " : " << Tpetra::version() << std::endl;
	
	// ベクトルのサイズ
	const int n = 8000000;

	// データの分散方法を指定（連続：各プロセスに均等に連続に割り当てる方法）
	auto node = Kokkos::DefaultNode::getDefaultNode();
	const int indexBase = 0;
	auto map = Teuchos::rcp(new Tpetra::Map<>(n, indexBase, comm, Tpetra::GloballyDistributed, node));

	// ベクトルxとyを作成
	auto x = Teuchos::rcp(new Tpetra::Vector<double>(map));
	auto y = Teuchos::rcp(new Tpetra::Vector<double>(map));
	
	// x = (  1,   1,   1, ...,   1)
	// y = (1/n, 1/n, 1/n, ..., 1/n)
	x->putScalar(1.0);
	y->putScalar(1.0/n);
	
	// z = x
	auto z = Teuchos::rcp(new Tpetra::Vector<double>(*x));
	
	// r = x・y
	const auto r = x->dot(*y);
	
	// x = αz + βx
	const double alpha = 3.14159;
	const double beta = 2.71828;
	x->update(alpha, *z, beta);
	
	// y = αx + βz + γy
	const double gamma = -10;
	y->update(alpha, *x, beta, *z, gamma);

	// norm = |y|
	const auto norm = y->norm2();
	
	// 最終結果表示
	if(rank == 0)
	{
		std::cout << "x・y= " << r << std::endl;
		std::cout << "y・y= " << norm << std::endl;

		system("pause");
	}

	return 0;
}
