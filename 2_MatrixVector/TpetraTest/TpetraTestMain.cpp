#include <Tpetra_DefaultPlatform.hpp>
#include <Tpetra_Version.hpp>
#include <Teuchos_GlobalMPISession.hpp>

#include <Tpetra_Vector.hpp>
#include <Tpetra_CrsMatrix.hpp>

#include <boost/mpi.hpp>

int main()
{
    // MPI環境＆コミュニケーター
    boost::mpi::environment env(true);
    boost::mpi::communicator mpiCommunicator;
	auto comm = Teuchos::rcp(new Teuchos::MpiComm<int>(Teuchos::opaqueWrapper(static_cast<MPI_Comm>(mpiCommunicator))));
	
	// MPIプロセスを表示
	const int size = mpiCommunicator.size();
	const int rank = mpiCommunicator.rank();
	std::cout << rank << "/" << size << " : " << Tpetra::version() << std::endl;
	
	// ベクトル・行列の要素数
	const int n = 20;
	const int maxNonZeroColumn = 10; // 行列の1行にある最大非ゼロ要素数（列数）

	// データの分散方法を指定（連続：各プロセスに均等に連続に割り当てる方法）
	auto node = Kokkos::DefaultNode::getDefaultNode();
	const int indexBase = 0;
	auto map = Teuchos::rcp(new Tpetra::Map<>(n, indexBase, comm, Tpetra::GloballyDistributed, node));

	// 行列Aを作成
	auto A = Teuchos::rcp(new Tpetra::CrsMatrix<double>(map, maxNonZeroColumn));
	auto getGlobalIndex = map->getNodeElementList();

	// ベクトルxとbを作成
	auto x = Teuchos::rcp(new Tpetra::Vector<double>(map));
	auto b = Teuchos::rcp(new Tpetra::Vector<double>(map));

	// 行列作成開始
	A->resumeFill();

	// 自プロセスの担当する要素で
	const int nLocal = map->getNodeNumElements();
	for(int iLocal = 0; iLocal < nLocal; iLocal++)
	{
		// 行列全体における行番号を取得
		const int i = getGlobalIndex[iLocal];

		// 2次中央差分の行列を作成する
		// （対角項が-2でその隣が1になる、↓こんな行列）
		// | -2  1  0  0  0  0  0  0 . . .  0  0  0|
		// |  1 -2  1  0  0  0  0  0 . . .  0  0  0|
		// |  0  1 -2  1  0  0  0  0 . . .  0  0  0|
		// |  0  0  1 -2  1  0  0  0 . . .  0  0  0|
		// |  0  0  0  1 -2  1  0  0 . . .  0  0  0|
		// |  0  0  0  0  1 -2  1  0 . . .  0  0  0|
		// |  0  0  0  0  0  1 -2  1 . . .  0  0  0|
		// |  0  0  0  0  0  0  1 -2 . . .  0  0  0|
		// |  .  .  .  .  .  .  .  . .      .  .  .|
		// |  .  .  .  .  .  .  .  .   .    .  .  .|
		// |  .  .  .  .  .  .  .  .     .  .  .  .|
		// |  0  0  0  0  0  0  0  0 . . . -2  1  0|
		// |  0  0  0  0  0  0  0  0 . . .  1 -2  1|
		// |  0  0  0  0  0  0  0  0 . . .  0  1 -2|

		// 0行目： a_00=-2, a_01=1
		if(i == 0)
		{
			A->insertGlobalValues(0, Teuchos::tuple(0, 1), Teuchos::tuple(-2.0, 1.0));
		}
		// n行目： a_n(n-1)=1, a_nn=-2
		else if(i == n-1)
		{
			A->insertGlobalValues(n-1, Teuchos::tuple(n-2, n-1), Teuchos::tuple(1.0, -2.0));
		}
		// i行目： a_i(i-1)=1, a_ii=-2, a_i(i+1)=1
		else
		{
			A->insertGlobalValues(i, Teuchos::tuple(i-1, i, i+1), Teuchos::tuple(1.0, -2.0, 1.0));
		}

		// b_i = 1.0
		b->replaceGlobalValue(i, 1.0);

		// x_i = (i-n)(i+1)/2 （Ax=bの解析解）
		x->replaceGlobalValue(i, (i-n)*(i+1)/2.0);
	}

	// 行列作成完了
	A->fillComplete();

	// Ax
	auto Ax = Teuchos::rcp(new Tpetra::Vector<double>(map));
	A->apply(*x, *Ax);

	// z = Ax-b
	auto z = Teuchos::rcp(new Tpetra::Vector<double>(map));
	z->update(1.0, *Ax, -1.0, *b, 0.0); // z = 1.0 Ax + (-1.0) b + 0.0 z

	// r = |z|
	const double r = z->norm2();
	
	// 最終結果表示
	if(rank == 0)
	{
		std::cout << "|Ax - b| = " << r << std::endl;
		system("pause");
	}

	return 0;
}
